var https = require('https');
var express = require('express');
var distance = require('./distance');
var weather = require('./weather');
var app = express();

// Google api information
var googleApiKey = 'AIzaSyD91eMalKfpaLo1XRr2OF6ATIA4Jb5G2g4';
var googleURL = 'https://maps.googleapis.com/maps/api/geocode/json?key=' + googleApiKey + '&address=';

// Addresses object
var addresses;
var response;

app.use(express.static(__dirname + "/public"));

app.get('/geo/:address1/:address2', function (req, res) {
    init();

    addresses.a1.text = req.params.address1.replace(/\s/g, '%20');
    addresses.a2.text = req.params.address2.replace(/\s/g, '%20');

    getAddressData(googleURL + addresses.a1.text);
    getAddressData(googleURL + addresses.a2.text);

    response = res;

    // res.send({ success: true });
});

app.listen(8080, function () {
    console.log('App listening on port 8080!');
});

// Function to get google information
function getAddressData(url, res) {
    var data = "";
    var execute = function(res) {
        res.on("data", function(chunk) {
            data += chunk;
        });

        res.on("end", function() {
            checkAndGetAgain(JSON.parse(data), res);
        });
    };

    var req = https.get(url, execute).end();
}

function checkAndGetAgain(data, res) {
    var location = data.results[0].geometry ? data.results[0].geometry.location : null;

    if(!addresses.a1.lat) {
        addresses.a1.lat = location.lat;
        addresses.a1.lng = location.lng;
    } else {
        addresses.a2.lat = location.lat;
        addresses.a2.lng = location.lng;
        var kmDistance = distance.calculateDistance(addresses);

        if(res) {
          weather.getWeather(addresses, {
            res: response,
            distance: kmDistance
          });
        }
    }
}

function init() {
  addresses = {
      a1:{
          text: '',
          lat: null,
          lng: null,
          wather: null
      },
      a2: {
          text: '',
          lat: null,
          lng: null,
          wather: null
      },
  };
}

exports = module.exports = app;
