'use strict';

/**
* Prototype para pasar a Radianes
*/
if(typeof Number.prototype.toRadians == 'undefined'){
    Number.prototype.toRadians = function() {
        return this * Math.PI / 180;
    }
}

/**
 * Función para calcular la distancia entre dos puntos.
 *
 * @param lat1 = Latitud del punto de origen
 * @param lat2 = Latitud del punto de destino
 * @param lng1 = Longitud del punto de origen
 * @param lng2 = Longitud del punto de destino
 */
function calcularDistancia(lat1, lat2, lng1, lng2){
    var R = 6371; // Radio del planeta tierra en km
    var phi1 = lat1.toRadians();
    var phi2 = lat2.toRadians();
    var deltaphi = (lat2-lat1).toRadians();
    var deltalambda = (lng2-lng1).toRadians();

    var a = Math.sin(deltaphi/2) * Math.sin(deltaphi/2) +
            Math.cos(phi1) * Math.cos(phi2) *
            Math.sin(deltalambda/2) * Math.sin(deltalambda/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

    var d = R * c
    return d;
}

exports.calculateDistance = function(addresses) {
    var location1 = addresses.a1;
    var location2 = addresses.a2;
    return calcularDistancia(location1.lat, location2.lat, location1.lng, location2.lng);
};
