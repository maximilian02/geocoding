var http = require('http');

var weatherUrl = 'http://api.openweathermap.org/data/2.5/weather?appid=24309213893424738f98cf3085325f23&units=metric&';

exports.getWeather = function(addresses, complete) {
    var locationUrl1 = weatherUrl + 'lat=' + addresses.a1.lat + '&lon=' + addresses.a1.lng;
    var locationUrl2 = weatherUrl + 'lat=' + addresses.a2.lat + '&lon=' + addresses.a2.lng;

    var execute1 = function(res) {
        var data1 = "";
        res.on("data", function(chunk) {
            data1 += chunk;
        });

        res.on("end", function() {
            addresses.a1.weather = JSON.parse(data1);
        });
    };

    var execute2 = function(res) {
        var data2 = "";
        res.on("data", function(chunk) {
            data2 += chunk;
        });

        res.on("end", function() {
            addresses.a2.weather = JSON.parse(data2);
            complete.res.send({
              data: addresses,
              distance: complete.distance
            });
        });
    };


    var req1 = http.get(locationUrl1, execute1).end();
    var req2 = http.get(locationUrl2, execute2).end();
};
